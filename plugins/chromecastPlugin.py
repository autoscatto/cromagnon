from fauxmo.plugins import FauxmoPlugin
import time
import pychromecast

class CastPlugin(FauxmoPlugin):
    state = "off"
    def __init__(self, *, name: str, port: int) -> None:
        DEVNAME = "Sala da pranzo"
        chromecasts = pychromecast.get_chromecasts()
        self.cast = next(cc for cc in chromecasts if cc.device.friendly_name == DEVNAME)
        self.cast.wait()
        print(self.cast.status)
        self.mc = self.cast.media_controller
        super().__init__(name=name, port=port)

    def on(self) -> bool:
        """Turn device on by calling `self.on_cmd` with `self.on_data`.
        Returns:
            True if the request seems to have been sent successfully
        """
        print('ON')
        self.mc.play_media('http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4', 'video/mp4')
        self.state = "on"
        return True

    def off(self) -> bool:
        """Turn device off by calling `self.off_cmd` with `self.off_data`.
        Returns:
            True if the request seems to have been sent successfully
        """
        print('OFF')
        self.mc.stop()
        self.state = "off"

        return True

    def get_state(self) -> str:
        print('STATE')

        return self.state